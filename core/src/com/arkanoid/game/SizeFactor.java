package com.arkanoid.game;

/**
 * Интерфейс для удобного и быстрого оперирования основными,
 * наиболее часто используемыми, значениями размеров в приложении
 */
public interface SizeFactor {
    float MAX_VIEW_HEIGHT = 480;
    float MAX_VIEW_WIDTH = 800;
    float PLATFORM_WIDTH = 64;
    float PLATFORM_HEIGHT = 10;
    float PLATFORM_LOWER_LIMIT_FACTOR = 35;
    float BALL_WIDTH = 18;
    float BALL_HEIGHT = 18;
    float BALL_LOWER_LIMIT_FACTOR = 170;
    float BLOCK_WIDTH = 40;
    float BLOCK_HEIGHT = 24;
}