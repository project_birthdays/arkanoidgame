package com.arkanoid.game.util;

import com.arkanoid.game.model.description.GameResultState;
import com.arkanoid.game.model.player.Goose;
import com.arkanoid.game.model.player.Wolf;
import com.arkanoid.game.render.game.GameUpdater;

public class WinnerDetection {
    private Wolf wolf;
    private Goose goose;
    private GameResultState gameResultState;

    public WinnerDetection(GameUpdater gameUpdater) {
        this.wolf = gameUpdater.getWolf();
        this.goose = gameUpdater.getGoose();
    }

    public GameResultState getGameResultState() {
        return this.gameResultState;
    }

    public void identifyResults() {
        if (isWolfMorePoints() | isWolfMoreHealthWithEqualPoints()
                | isWolfMoreRuntimeWithEqualsPointsAndHealth()) {
            this.gameResultState = GameResultState.WOLF_WON;
        } else if (isGooseMorePoints() | isGooseMoreHealthWithEqualPoints()
                | isGooseMoreRuntimeWithEqualsPointsAndHealth()) {
            this.gameResultState = GameResultState.GOOSE_WON;
        } else {
            this.gameResultState = GameResultState.DRAW;
        }
    }

    private boolean isWolfMorePoints() {
        return this.wolf.getPoints() > this.goose.getPoints();
    }
    private boolean isGooseMorePoints() {
        return this.goose.getPoints() > this.wolf.getPoints();
    }
    private boolean isWolfMoreHealthWithEqualPoints() {
        return isEqualPoints() && isWolfMoreHealth();
    }
    private boolean isGooseMoreHealthWithEqualPoints() {
        return isEqualPoints() && isGooseMoreHealth();
    }
    private boolean isWolfMoreRuntimeWithEqualsPointsAndHealth() {
        return isEqualsPointsAndHealth() && isWolfMoreRuntime();
    }
    private boolean isGooseMoreRuntimeWithEqualsPointsAndHealth() {
        return isEqualsPointsAndHealth() && isGooseMoreRuntime();
    }
    private boolean isWolfMoreHealth() {
        return this.wolf.getHealth() > this.goose.getHealth();
    }
    private boolean isGooseMoreHealth() {
        return this.goose.getHealth() > this.wolf.getHealth();
    }
    private boolean isWolfMoreRuntime() {
        return this.wolf.getRuntimeInInt() > this.goose.getRuntimeInInt();
    }
    private boolean isGooseMoreRuntime() {
        return this.goose.getRuntimeInInt() > this.wolf.getRuntimeInInt();
    }
    private boolean isEqualPoints() {
        return this.goose.getPoints() == this.wolf.getPoints();
    }
    private boolean isEqualsPointsAndHealth() {
        return isEqualPoints() && this.goose.getHealth() == this.wolf.getHealth();
    }
}