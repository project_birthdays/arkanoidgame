package com.arkanoid.game.util;

import com.arkanoid.game.model.description.PlayerState;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.InputProcessor;

public class InputHandler implements InputProcessor {
    private GameUpdater gameUpdater;
    private PlayerState wolfState, gooseState;

    public InputHandler(GameUpdater gameUpdater){
        this.gameUpdater = gameUpdater;
        this.wolfState = gameUpdater.getWolf().getPlayerState();
        this.gooseState = gameUpdater.getGoose().getPlayerState();
    }

    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        if (gameUpdater.getGameState().isPrep() && (wolfState.isBegins() | gooseState.isBegins())) {
            gameUpdater.doPlay();
        } else if(gameUpdater.getGameState().isPrep() && ((wolfState.isPlayed() && gooseState.isReady())
                | (gooseState.isPlayed() && wolfState.isReady()))) {
            gameUpdater.doPlay();
        }
        if (gameUpdater.getGameState().isPause()) {
            gameUpdater.doCancelPause();
        } else if (gameUpdater.getGameState().isCompletion()) {
            gameUpdater.doChangePlayer();
        } else if(gameUpdater.getGameState().isResult()){
            gameUpdater.doRestart();
        }
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
