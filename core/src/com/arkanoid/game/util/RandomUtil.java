package com.arkanoid.game.util;

import com.badlogic.gdx.math.MathUtils;

public class RandomUtil {
    public static byte getRandomValueOfPair() {
        return (byte) MathUtils.random(0, 1);
    }
}