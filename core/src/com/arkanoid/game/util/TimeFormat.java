package com.arkanoid.game.util;

public class TimeFormat {
    private static final int MIN = 60;

    public static String getTimeInString(float value) {
        return String.format("%x:%s",
                ((int) value) / MIN % MIN,
                getSecondsFormat((int) value % MIN));
    }

    private static String getSecondsFormat(int seconds) {
        return String.format("%s", (seconds > 9) ? seconds : "0" + seconds);
    }
}