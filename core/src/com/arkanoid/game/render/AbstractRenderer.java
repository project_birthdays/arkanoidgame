package com.arkanoid.game.render;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Абстрактный класс, предназначенный для отрисовки объектов экрана
 */
public abstract class AbstractRenderer {
    private static final String INTERNAL_FONT_PATH = "font/verdana_font.fnt";
    protected SpriteBatch spriteBatch;
    protected BitmapFont bitmapFont;

    protected AbstractRenderer() {
        this.spriteBatch = new SpriteBatch();
        this.bitmapFont = new BitmapFont(Gdx.files.internal(INTERNAL_FONT_PATH));
    }

    public abstract void draw();
    public abstract void dispose();
}