package com.arkanoid.game.render;

import com.badlogic.gdx.graphics.OrthographicCamera;

/**
 * Абстрактный класс, предназначенный для обновления положений объектов экрана
 */
public abstract class AbstractUpdater {
    protected OrthographicCamera orthographicCamera;

    protected AbstractUpdater(){
        this.orthographicCamera = new OrthographicCamera();
    }

    public OrthographicCamera getOrthographicCamera() {
        return orthographicCamera;
    }

    public abstract void update(float delta);
}