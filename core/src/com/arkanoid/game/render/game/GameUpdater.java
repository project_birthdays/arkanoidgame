package com.arkanoid.game.render.game;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.ScreenElementListener;
import com.arkanoid.game.controller.AbstractDynamicController;
import com.arkanoid.game.controller.game.BallController;
import com.arkanoid.game.controller.game.BlockController;
import com.arkanoid.game.controller.game.GameBgController;
import com.arkanoid.game.controller.game.PlatformController;
import com.arkanoid.game.controller.player.GooseController;
import com.arkanoid.game.controller.player.WolfController;
import com.arkanoid.game.model.description.CompletionPlayerState;
import com.arkanoid.game.model.description.GameState;
import com.arkanoid.game.model.game.Ball;
import com.arkanoid.game.model.game.Block;
import com.arkanoid.game.model.game.Platform;
import com.arkanoid.game.model.general.BackgroundPicture;
import com.arkanoid.game.model.player.Goose;
import com.arkanoid.game.model.player.Wolf;
import com.arkanoid.game.render.AbstractUpdater;
import com.arkanoid.game.util.WinnerDetection;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

import java.util.Arrays;
import java.util.List;

/**
 * Класс предназначен для обновления объектов экрана игры
 *
 * В классе содержатся и единожды инициализируются все объекты,
 * находящиеся на игровом экране приложения, для дальнейшего оперирования ими
 */
public class GameUpdater extends AbstractUpdater implements SizeFactor {
    private static final String INTERNAL_BG_PIC_PATH = "image/game_background.png";
    private static final String INTERNAL_HEART_PIC_PATH = "image/heart.png";
    private static final String INTERNAL_BUTTON_PIC_LEFT = "image/button_left.png";
    private static final String INTERNAL_BUTTON_PIC_RIGHT = "image/button_right.png";
    private static final int MAX_WIGHT_HEART = 50, MAX_HEIGHT_HEART = 50;
    private static final int MAX_HEIGHT_BUTTON = 90, MAX_WIGHT_BUTTON = 90;

    private Sprite buttonLeft, buttonRight;
    private BackgroundPicture[] backgroundPictures;
    private Goose goose;
    private Wolf wolf;
    private Platform platform;
    private Ball ball;
    private Array<Block> blockArray;
    private Pool<Block> blockPool = Pools.get(Block.class);

    private GameState gameState, prevPauseGameState;
    private CompletionPlayerState completionPlayerState;
    private GameEvents gameEvents;
    private WinnerDetection winnerDetection;
    private BallController ballController;
    private List<ScreenElementListener> staticDrawControllersList;
    private List<AbstractDynamicController> dynamicControllerList;
    private List<Sprite> spriteList;

    public GameUpdater(){
        super();
        this.gameState = GameState.PREP;
        this.completionPlayerState = CompletionPlayerState.NONE;
        this.goose = new Goose();
        this.wolf = new Wolf();
        super.orthographicCamera.setToOrtho(false, MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT);
        this.gameEvents = new GameEvents(this);
        this.winnerDetection = new WinnerDetection(this);
        this.gameEvents.beginsRandomPlayer();

        this.backgroundPictures = new BackgroundPicture[2];
        this.backgroundPictures[0] = new BackgroundPicture(new Texture(INTERNAL_BG_PIC_PATH), 0, 0);
        this.backgroundPictures[1] = new BackgroundPicture(new Texture(INTERNAL_BG_PIC_PATH), 800, 0);
        Sprite heartSprite = new Sprite(new Texture(INTERNAL_HEART_PIC_PATH));
        buttonLeft = new Sprite(new Texture(INTERNAL_BUTTON_PIC_LEFT));
        buttonRight = new Sprite(new Texture(INTERNAL_BUTTON_PIC_RIGHT));
        heartSprite.setSize(MAX_WIGHT_HEART, MAX_HEIGHT_HEART);
        heartSprite.setPosition(MAX_VIEW_WIDTH / 7.6f, MAX_VIEW_HEIGHT / 1.25f);
        buttonLeft.setSize(MAX_WIGHT_BUTTON, MAX_HEIGHT_BUTTON);
        buttonLeft.setPosition(MAX_VIEW_WIDTH / 33.6f, MAX_VIEW_HEIGHT / 7.25f);
        buttonRight.setSize(MAX_WIGHT_BUTTON, MAX_HEIGHT_BUTTON);
        buttonRight.setPosition(MAX_VIEW_WIDTH / 1.19f, MAX_VIEW_HEIGHT / 7.25f);

        this.blockArray = new Array<>();
        this.platform = new Platform(MAX_VIEW_WIDTH / 2 - PLATFORM_WIDTH / 2,
                PLATFORM_LOWER_LIMIT_FACTOR, PLATFORM_WIDTH, PLATFORM_HEIGHT);
        this.ball = new Ball(MAX_VIEW_WIDTH / 2 - BALL_WIDTH / 2,
                BALL_LOWER_LIMIT_FACTOR, BALL_WIDTH, BALL_HEIGHT);
        GameBgController bgController = new GameBgController(this);
        BlockController blockController = new BlockController(this);
        blockController.initBlocks();
        PlatformController platformController = new PlatformController(this);
        this.ballController = new BallController(this);
        WolfController wolfController = new WolfController(this);
        GooseController gooseController = new GooseController(this);

        this.staticDrawControllersList = Arrays.asList(bgController, wolfController, gooseController);
        this.dynamicControllerList = Arrays.asList(blockController, platformController, ballController);
        this.spriteList = Arrays.asList(heartSprite, buttonLeft, buttonRight);
    }

    public GameState getGameState() {
        return this.gameState;
    }
    void setGameState(GameState gameState) {
        this.gameState = gameState;
    }
    public GameState getPrevPauseGameState() {
        return this.prevPauseGameState;
    }
    CompletionPlayerState getCompletionPlayerState() {
        return this.completionPlayerState;
    }
    void setCompletionPlayerState(CompletionPlayerState completionPlayerState) {
        this.completionPlayerState = completionPlayerState;
    }
    WinnerDetection getWinnerDetection() {
        return this.winnerDetection;
    }
    public GameEvents getGameEvents() {
        return gameEvents;
    }
    List<Sprite> getSpriteList() {
        return this.spriteList;
    }
    public Wolf getWolf() {
        return this.wolf;
    }
    public Goose getGoose() {
        return this.goose;
    }
    public BackgroundPicture[] getBackgroundPictures() {
        return this.backgroundPictures;
    }
    public Pool<Block> getBlockPool() {
        return this.blockPool;
    }
    public Platform getPlatform() {
        return this.platform;
    }
    public Array<Block> getBlockArray() {
        return this.blockArray;
    }
    public Ball getBall() {
        return this.ball;
    }
    BallController getBallController() {
        return ballController;
    }
    List<ScreenElementListener> getStaticDrawControllersList() {
        return this.staticDrawControllersList;
    }
    List<AbstractDynamicController> getDynamicControllerList() {
        return this.dynamicControllerList;
    }

    @Override
    public void update(float delta){
        switch (gameState) {
            case RUNNING:
                updateRunning(delta);
                break;
            case CHANGE_PLAYER:
                resetRoundProp();
                break;
            case RESTART:
                restartRound();
                break;
            default:
                break;
        }
    }

    private void updateRunning(float delta) {
        for (AbstractDynamicController dynamicController : dynamicControllerList) {
            dynamicController.updateElement(delta);
        }
        for (ScreenElementListener controller : staticDrawControllersList) {
            controller.updateElement(delta);
        }
        gameEvents.reduceRoundTime(delta);
        gameEvents.increasePlayerRuntime(delta);
        gameEvents.trackCompletionRound();
    }

    private void resetRoundProp() {
        if ((goose.getPlayerState().isPlayed() && wolf.getPlayerState().isReady())
                | (wolf.getPlayerState().isPlayed() && goose.getPlayerState().isReady())) {
            gameEvents.restartDynamicModels();
            gameEvents.resetRoundTime();
            completionPlayerState = CompletionPlayerState.NONE;
        }
        gameState = GameState.PREP;
    }
    private void restartRound() {
        gameEvents.restartDynamicModels();
        gameEvents.resetRoundTime();
        goose.updateProperties();
        wolf.updateProperties();
        gameEvents.beginsRandomPlayer();
        gameState = GameState.PREP;
    }

    public void doPlay() {
        gameState = GameState.RUNNING;
        gameEvents.doPlayRandomPlayer();
        gameEvents.doPlaySecondPlayer();
    }
    public void doChangePlayer() {
        gameState = GameState.CHANGE_PLAYER;
        gameEvents.doResult();
    }
    public void doPause() {
        if (!gameState.isPause()){
            prevPauseGameState = gameState;
        }
        gameState = GameState.PAUSE;
    }
    public void doCancelPause() {
        gameState = prevPauseGameState;
    }
    public void doRestart() {
        gameState = GameState.RESTART;
    }

    public boolean isLeftBorderOnPlatform() {
        return ball.getX() < ((platform.getX()) + (PLATFORM_WIDTH / 2)) && ball.getX() + ball.getWidth() > (platform.getX())
                && ball.getY() < (platform.getY()) + ((platform.getHeight() * 1.15f)) && ball.getY() + ball.getHeight() > (platform.getY() + (platform.getHeight() * 1.15f));
    }
    public boolean isRightBorderOnPlatform() {
        return ball.getX() < (platform.getX() + (PLATFORM_WIDTH / 2 )) + (PLATFORM_WIDTH / 2) && ball.getX() + ball.getWidth() > (platform.getX() + (PLATFORM_WIDTH / 2 ))
                && ball.getY() < (platform.getY()) + ((platform.getHeight() * 1.15f)) && ball.getY() + ball.getHeight() > (platform.getY() + (platform.getHeight() * 1.15f));
    }
    public boolean isUpBorderOnPlatform() {
        return ball.getX() < (platform.getX()) + (platform.getWidth()) && ball.getX() + ball.getWidth() > (platform.getX())
                && ball.getY() < (platform.getY()) + ((platform.getHeight() * 1.15f)) && ball.getY() + ball.getHeight() > (platform.getY() + (platform.getHeight() * 1.15f));
    }
    public boolean isLeftBorderOnScreen() {
        return  ball.getX() < 0;
    }
    public boolean isRightBorderOnScreen() {
        return ball.getX() + BALL_WIDTH > MAX_VIEW_WIDTH;
    }
    public boolean isUpBorderOnScreen() {
        return ball.getY() + BALL_HEIGHT > MAX_VIEW_HEIGHT;
    }
    public boolean isUpBlockCollision(Ball ball, Block block) {
        return ball.getX() < (block.getX()) + (block.getWidth()) && ball.getX() + ball.getWidth() > (block.getX())
                && ball.getY() < (block.getY()) + (block.getHeight()) && ball.getY() + ball.getHeight() > (block.getY() + block.getHeight());
    }
    public boolean isDownBlockCollision(Ball ball, Block block) {
        return ball.getX() < (block.getX()) + (block.getWidth()) && ball.getX() + ball.getWidth() > (block.getX())
                && ball.getY() < (block.getY()) && ball.getY() + ball.getHeight() > (block.getY());
    }
    public boolean isLeftBlockCollision(Ball ball, Block block) {
        return ball.getX() < (block.getX()) && ball.getX() + ball.getWidth() > (block.getX())
                && ball.getY() < (block.getY() + 0.90f) + (block.getHeight() - 0.90f) && ball.getY() + ball.getHeight() > (block.getY() + 0.90f);
    }
    public boolean isRightBlockCollision(Ball ball, Block block) {
        return ball.getX() < (block.getX()) + (block.getWidth()) && ball.getX() + ball.getWidth() > (block.getX()) + (block.getWidth())
                && ball.getY() < (block.getY() + 0.90f) + (block.getHeight() - 0.90f) && ball.getY() + ball.getHeight() > (block.getY() + 0.90f);
    }
    public boolean isLeftButton(float x, float y){
        return ((x >= buttonLeft.getX()) && x <= (buttonLeft.getX() + buttonLeft.getWidth())
                && (y >= buttonLeft.getY()) && y <= (buttonLeft.getY() + buttonLeft.getHeight()));
    }
    public boolean isRightButton(float x, float y){
        return ((x >= buttonRight.getX()) && x <= (buttonRight.getX() + buttonRight.getWidth())
                && (y >= buttonRight.getY()) && y <= (buttonRight.getY() + buttonRight.getHeight()));
    }
}