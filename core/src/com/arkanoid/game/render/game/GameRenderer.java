package com.arkanoid.game.render.game;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.AbstractDynamicController;
import com.arkanoid.game.controller.ScreenElementListener;
import com.arkanoid.game.render.AbstractRenderer;
import com.arkanoid.game.util.TimeFormat;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * Класс предназначен для отрисовки объектов экрана игры
 *
 * Доступ к объетам игрового экрана приложения получается через
 * экземпляр класса GameUpdater, в котором они хранятся
 */
public class GameRenderer extends AbstractRenderer implements SizeFactor {
    private OrthographicCamera camera;
    private GameUpdater gameUpdater;
    private GameEvents gameEvents;

    public GameRenderer(GameUpdater gameUpdater) {
        super();
        this.gameUpdater = gameUpdater;
        this.camera = gameUpdater.getOrthographicCamera();
        this.gameEvents = gameUpdater.getGameEvents();
    }

    @Override
    public void draw() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        super.spriteBatch.setProjectionMatrix(this.camera.combined);
        super.spriteBatch.begin();

        for (ScreenElementListener controller : gameUpdater.getStaticDrawControllersList()) {
            controller.drawElement(super.spriteBatch);
        }

        if (gameUpdater.getGameState().isPrep() | gameUpdater.getGameState().isRunning() | (gameUpdater.getGameState().isPause()
                && !(gameUpdater.getPrevPauseGameState().isCompletion() | gameUpdater.getPrevPauseGameState().isResult()))){
            for (AbstractDynamicController dynamicController : gameUpdater.getDynamicControllerList()) {
                dynamicController.drawElement(super.spriteBatch);
            }
        }
        if (gameUpdater.getGameState().isPause()) {
            bitmapFont.draw(spriteBatch, "ПАУЗА", MAX_VIEW_WIDTH / 2.30f, MAX_VIEW_HEIGHT / 1.15f);
            bitmapFont.draw(spriteBatch, "Нажмите на экран, чтобы продолжить!", MAX_VIEW_WIDTH / 12.5f, MAX_VIEW_HEIGHT / 2.1f);
        }
        if (!gameUpdater.getGameState().isResult() & !(gameUpdater.getGameState().isPause() && (gameUpdater.getPrevPauseGameState().isResult() | gameUpdater.getPrevPauseGameState().isCompletion()))
                & !(gameUpdater.getGameState().isCompletion())) {
            gameUpdater.getSpriteList().get(0).draw(spriteBatch);
            if (gameUpdater.getGameState().isRunning()) {
                super.bitmapFont.draw(spriteBatch, TimeFormat.getTimeInString(gameEvents.getRoundTime()), MAX_VIEW_WIDTH / 2.2f, MAX_VIEW_HEIGHT / 1.15f);
            }
        }
        if (gameUpdater.getGameState().isResult()) {
            super.bitmapFont.draw(spriteBatch, gameUpdater.getWolf().toString(), MAX_VIEW_WIDTH / 11.0f, MAX_VIEW_HEIGHT / 1.4f);
            super.bitmapFont.draw(spriteBatch, gameUpdater.getGoose().toString(), MAX_VIEW_WIDTH / 11.0f, MAX_VIEW_HEIGHT / 1.6f);
            super.bitmapFont.draw(spriteBatch, "Нажмите на экран, чтобы переиграть!", MAX_VIEW_WIDTH / 11f, MAX_VIEW_HEIGHT / 2.1f);

            if (gameUpdater.getWinnerDetection().getGameResultState().isGooseWon()) {
                bitmapFont.draw(spriteBatch, "Выиграл гусь!", MAX_VIEW_WIDTH / 3.f, MAX_VIEW_HEIGHT / 1.15f);
            } else if (gameUpdater.getWinnerDetection().getGameResultState().isWolfWon()) {
                bitmapFont.draw(spriteBatch, "Выиграл волк!", MAX_VIEW_WIDTH / 3.f, MAX_VIEW_HEIGHT / 1.15f);
            } else {
                bitmapFont.draw(spriteBatch, "Ничья!", MAX_VIEW_WIDTH / 2.35f, MAX_VIEW_HEIGHT / 1.15f);
            }
        }
        if (gameUpdater.getGameState().isCompletion()) {
            if (gameUpdater.getCompletionPlayerState().isNoBlocks()) {
                bitmapFont.draw(spriteBatch, "Все блоки разрушены!", MAX_VIEW_WIDTH / 3.85f, MAX_VIEW_HEIGHT / 1.15f);
            } else if (gameUpdater.getCompletionPlayerState().isNoHealth()) {
                bitmapFont.draw(spriteBatch, "Жизни кончились!", MAX_VIEW_WIDTH / 3.35f, MAX_VIEW_HEIGHT / 1.15f);
            } else {
                bitmapFont.draw(spriteBatch, "Время кончилось!", MAX_VIEW_WIDTH / 3.35f, MAX_VIEW_HEIGHT / 1.15f);
            }
            bitmapFont.draw(spriteBatch, "Нажмите на экран, чтобы продолжить!", MAX_VIEW_WIDTH / 12.5f, MAX_VIEW_HEIGHT / 2.1f);
        }
        gameUpdater.getSpriteList().get(1).draw(spriteBatch);
        gameUpdater.getSpriteList().get(2).draw(spriteBatch);
        super.spriteBatch.end();
    }

    @Override
    public void dispose(){
        for (AbstractDynamicController dynamicController : gameUpdater.getDynamicControllerList()) {
            dynamicController.disposeElement();
        }
        for (ScreenElementListener staticController : gameUpdater.getStaticDrawControllersList()) {
            staticController.disposeElement();
        }
        for (Sprite sprite : gameUpdater.getSpriteList()) {
            sprite.getTexture().dispose();
        }
        for (Sound sound : gameEvents.getSoundList()) {
            sound.dispose();
        }
        super.bitmapFont.dispose();
        super.spriteBatch.dispose();
    }
}