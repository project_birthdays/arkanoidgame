package com.arkanoid.game.render.game;

import com.arkanoid.game.controller.AbstractDynamicController;
import com.arkanoid.game.model.description.CompletionPlayerState;
import com.arkanoid.game.model.description.GameState;
import com.arkanoid.game.model.description.PlayerState;
import com.arkanoid.game.model.game.Block;
import com.arkanoid.game.model.player.Goose;
import com.arkanoid.game.model.player.Wolf;
import com.arkanoid.game.util.RandomUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.Arrays;
import java.util.List;

public class GameEvents {
    private static final float DEFAULT_ROUND_TIME = 105.5f;
    private static final String INTERNAL_DESTROY_SOUND_PATH = "sound/destroy.wav";
    private static final String INTERNAL_FALLING_SOUND_PATH = "sound/falling.wav";
    private static final String INTERNAL_BORDER_SOUND_PATH = "sound/border.wav";

    private GameUpdater gameUpdater;
    private Wolf wolf;
    private Goose goose;
    private Sound destroyBlockSound, fallingBallSound, borderSound, completeSound, resultSound;
    private List<Sound> soundList;
    private float roundTime;

    GameEvents(GameUpdater gameUpdater) {
        this.gameUpdater = gameUpdater;
        this.wolf = gameUpdater.getWolf();
        this.goose = gameUpdater.getGoose();
        this.roundTime = DEFAULT_ROUND_TIME;
        this.destroyBlockSound = Gdx.audio.newSound(Gdx.files.internal(INTERNAL_DESTROY_SOUND_PATH));
        this.fallingBallSound = Gdx.audio.newSound(Gdx.files.internal(INTERNAL_FALLING_SOUND_PATH));
        this.borderSound = Gdx.audio.newSound(Gdx.files.internal(INTERNAL_BORDER_SOUND_PATH));
        this.soundList = Arrays.asList(destroyBlockSound, fallingBallSound, borderSound);
    }

    List<Sound> getSoundList() {
        return soundList;
    }
    float getRoundTime() {
        return this.roundTime;
    }

    void beginsRandomPlayer() {
        byte startPlayerFactor =  RandomUtil.getRandomValueOfPair();
        if (startPlayerFactor == 1) {
            this.wolf.setPlayerState(PlayerState.BEGINS);
        } else {
            this.goose.setPlayerState(PlayerState.BEGINS);
        }
    }
    void trackCompletionRound() {
        if (gameUpdater.getBallController().isLowFactorPosition()) {
            ballNoCollisionFactor();
        }
        trackCompletionPlayer();
    }
    void doResult() {
        if (goose.getPlayerState().isPlayed() && wolf.getPlayerState().isPlayed()) {
            gameUpdater.getWinnerDetection().identifyResults();
            gameUpdater.setGameState(GameState.RESULT);
        }
    }
    void doPlayRandomPlayer() {
        if (wolf.getPlayerState().isBegins()) {
            wolf.setPlayerState(PlayerState.PLAYING);
        } else if (goose.getPlayerState().isBegins()){
            goose.setPlayerState(PlayerState.PLAYING);
        }
    }
    void doPlaySecondPlayer() {
        if (wolf.getPlayerState().isPlayed() && goose.getPlayerState().isReady()) {
            goose.setPlayerState(PlayerState.PLAYING);
        } else if (goose.getPlayerState().isPlayed() && wolf.getPlayerState().isReady()) {
            wolf.setPlayerState(PlayerState.PLAYING);
        }
    }
    void restartDynamicModels() {
        for (AbstractDynamicController dynamicController : gameUpdater.getDynamicControllerList()) {
            dynamicController.onRestart();
        }
    }

    public void borderCollisionFactor() {
        borderSound.play();
    }
    public void blockCollisionFactor(Block block) {
        block.setDestroyed(true);
        destroyBlockSound.play();
        increasePlayerPoints(block.getPoints());
    }
    void resultStateFactor() {
        resultSound.play();
    }
    void completionStateFactor() {
        completeSound.play();
    }
    void increasePlayerRuntime(float delta) {
        if (goose.getPlayerState().isPlaying()) {
            goose.increaseRunTime(delta);
        } else {
            wolf.increaseRunTime(delta);
        }
    }
    void reduceRoundTime(float delta) {
        this.roundTime -= delta;
    }
    void resetRoundTime() {
        this.roundTime = DEFAULT_ROUND_TIME;
    }

    private void ballNoCollisionFactor() {
        reducePlayerHealth();
        fallingBallSound.play();
        gameUpdater.getBall().resetPosition();
    }
    private void trackCompletionPlayer() {
        if (goose.getPlayerState().isPlaying() && isCompleteGooseRound()) {
            setCompletionStateByAction();
            gameUpdater.setGameState(GameState.COMPLETION);
            goose.setPlayerState(PlayerState.PLAYED);
        } else if (wolf.getPlayerState().isPlaying() && isCompleteWolfRound()){
            setCompletionStateByAction();
            gameUpdater.setGameState(GameState.COMPLETION);
            wolf.setPlayerState(PlayerState.PLAYED);
        }
    }
    private void reducePlayerHealth() {
        if (this.goose.getPlayerState().isPlaying() && this.gameUpdater.getBallController().isLowFactorPosition()) {
            this.goose.reduceHealth();
        } else {
            this.wolf.reduceHealth();
        }
    }
    private void increasePlayerPoints(int points) {
        if (this.goose.getPlayerState().isPlaying()) {
            this.goose.increasePoints(points);
        } else {
            this.wolf.increasePoints(points);
        }
    }
    private void setCompletionStateByAction() {
        if (this.goose.isNoPlayerHealth() | this.wolf.isNoPlayerHealth()) {
            gameUpdater.setCompletionPlayerState(CompletionPlayerState.NO_HEALTH);
        } else if (gameUpdater.getBlockArray().isEmpty()) {
            gameUpdater.setCompletionPlayerState(CompletionPlayerState.NO_BLOCKS);
        } else if (this.roundTime < 0.f) {
            gameUpdater.setCompletionPlayerState(CompletionPlayerState.NO_ROUND_TIME);
        }
    }
    private boolean isCompleteGooseRound() {
        return this.goose.isNoPlayerHealth() | this.gameUpdater.getBlockArray().isEmpty() | this.roundTime < 0.f;
    }
    private boolean isCompleteWolfRound() {
        return this.wolf.isNoPlayerHealth() | this.gameUpdater.getBlockArray().isEmpty() | this.roundTime < 0.f;
    }
}