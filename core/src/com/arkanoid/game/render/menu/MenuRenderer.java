package com.arkanoid.game.render.menu;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.render.AbstractRenderer;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class MenuRenderer extends AbstractRenderer implements SizeFactor {
    private MenuUpdater menuUpdater;

    public MenuRenderer(MenuUpdater menuUpdater) {
        this.menuUpdater = menuUpdater;
    }

    @Override
    public void draw() {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        spriteBatch.setProjectionMatrix(menuUpdater.getOrthographicCamera().combined);
        spriteBatch.begin();
        menuUpdater.getBackgroundController().drawElement(spriteBatch);
        for (Sprite sprite : menuUpdater.getSpriteList()) {
            sprite.draw(spriteBatch);
        }
        spriteBatch.end();
    }

    @Override
    public void dispose(){
        menuUpdater.getBackgroundController().disposeElement();
        for (Sprite sprite : menuUpdater.getSpriteList()) {
            sprite.getTexture().dispose();
        }
        super.bitmapFont.dispose();
        super.spriteBatch.dispose();
    }
}