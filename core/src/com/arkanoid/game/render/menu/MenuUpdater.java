package com.arkanoid.game.render.menu;

import com.arkanoid.game.ArkanoidGame;
import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.general.BackgroundController;
import com.arkanoid.game.model.general.BackgroundPicture;
import com.arkanoid.game.render.AbstractUpdater;
import com.arkanoid.game.screen.GameScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector3;

import java.util.Arrays;
import java.util.List;


public class MenuUpdater extends AbstractUpdater implements  SizeFactor {
    private static final String INTERNAL_PICTURE_PATH = "image/menu_background.jpg";
    private static final String GAME_TITLE_PNG = "image/game_title.png";
    private static final String PLAY_BUTTON_IMG = "image/button_game.png";
    private static final String EXIT_BUTTON_IMG = "image/button_exit.png";
    private static final int MAX_WIGHT_TITLE = 250, MAX_HEIGHT_TITLE = 150;
    private static final float PLAY_BTN_HEIGHT_SIZE = 250, PLAY_BTN_WIDTH_SIZE = 400;

    private BackgroundPicture backgroundPicture;
    private BackgroundController backgroundController;

    private Sprite startButtonSprite, exitButtonSprite;
    private List<Sprite> spriteList;
    private Vector3 tempCoordinates;
    private ArkanoidGame arkanoidGame;

    public MenuUpdater(ArkanoidGame arkanoidGame){
        this.arkanoidGame = arkanoidGame;
        super.orthographicCamera.setToOrtho(false, MAX_VIEW_WIDTH, MAX_VIEW_HEIGHT);
        backgroundPicture = new BackgroundPicture(new Texture(INTERNAL_PICTURE_PATH), 0, 0);
        backgroundController = new BackgroundController(this);

        Sprite titleButtonSprite = new Sprite(new Texture(GAME_TITLE_PNG));
        startButtonSprite = new Sprite(new Texture(PLAY_BUTTON_IMG));
        exitButtonSprite = new Sprite(new Texture(EXIT_BUTTON_IMG));
        titleButtonSprite.setSize(MAX_WIGHT_TITLE, MAX_HEIGHT_TITLE);
        startButtonSprite.setSize(PLAY_BTN_WIDTH_SIZE, PLAY_BTN_HEIGHT_SIZE);
        exitButtonSprite.setSize(PLAY_BTN_WIDTH_SIZE, PLAY_BTN_HEIGHT_SIZE);
        titleButtonSprite.setPosition(MAX_VIEW_WIDTH / 2.9f , MAX_VIEW_HEIGHT / 1.4f);
        startButtonSprite.setPosition(MAX_VIEW_WIDTH / 3.8f , MAX_VIEW_HEIGHT / 2.8f);
        exitButtonSprite.setPosition(MAX_VIEW_WIDTH / 3.8f , MAX_VIEW_HEIGHT / 45.9f);
        spriteList = Arrays.asList(titleButtonSprite, startButtonSprite, exitButtonSprite);
        tempCoordinates = new Vector3();
    }
    public BackgroundPicture getBackgroundPicture() {
        return backgroundPicture;
    }
    BackgroundController getBackgroundController() {
        return backgroundController;
    }
    List<Sprite> getSpriteList() {
        return spriteList;
    }

    public void update(float delta){
        orthographicCamera.update();
        backgroundController.updateElement(delta);
        handleTouch();
    }

    private void handleTouch() {
        if (Gdx.input.justTouched()) {
            tempCoordinates.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            orthographicCamera.unproject(tempCoordinates);
            float touchX = tempCoordinates.x;
            float touchY = tempCoordinates.y;
            if((touchX >= startButtonSprite.getX() + 150) && touchX <= (startButtonSprite.getX() + startButtonSprite.getWidth() - 150)
                    && (touchY >= startButtonSprite.getY() + 50) && touchY <= (startButtonSprite.getY() + startButtonSprite.getHeight() - 100) ){
                arkanoidGame.setScreen(new GameScreen());
            }
            else if((touchX>=exitButtonSprite.getX() + 150) && touchX<= (exitButtonSprite.getX()+exitButtonSprite.getWidth() - 150)
                    && (touchY>=exitButtonSprite.getY() + 50) && touchY<=(exitButtonSprite.getY()+exitButtonSprite.getHeight() - 100) ){
                Gdx.app.exit();
            }
        }
    }
}