package com.arkanoid.game.screen;


import com.arkanoid.game.ArkanoidGame;
import com.arkanoid.game.render.menu.MenuRenderer;
import com.arkanoid.game.render.menu.MenuUpdater;
import com.badlogic.gdx.Screen;

public class MenuScreen implements Screen {
    private MenuRenderer menuRenderer;
    private MenuUpdater menuUpdater;

    public MenuScreen(ArkanoidGame arkanoidGame) {
        this.menuUpdater = new MenuUpdater(arkanoidGame);
        this.menuRenderer = new MenuRenderer(menuUpdater);
    }

    @Override
    public void show() {}

    @Override
    public void render(float delta) {
        this.menuRenderer.draw();
        this.menuUpdater.update(delta);
    }

    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {}

    @Override
    public void resume() {}

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        this.menuRenderer.dispose();
    }
}