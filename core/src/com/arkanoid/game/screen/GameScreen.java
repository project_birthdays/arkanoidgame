package com.arkanoid.game.screen;

import com.arkanoid.game.render.game.GameRenderer;
import com.arkanoid.game.render.game.GameUpdater;
import com.arkanoid.game.util.InputHandler;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;

/**
 * Класс представляет из себя игровой экран приложения, и отвечает за его
 * различные состояния
 *
 * Отрисовка и обновление объектов на экране происходит благодаря методам
 * вспомогательных классов GameRenderer и GameUpdater, соответственно
 */
public class GameScreen implements Screen {
    private GameRenderer gameRenderer;
    private GameUpdater gameUpdater;

    public GameScreen() {
        this.gameUpdater = new GameUpdater();
        this.gameRenderer = new GameRenderer(gameUpdater);
        Gdx.input.setInputProcessor(new InputHandler(gameUpdater));
    }

    @Override
    public void show() {}

    @Override
    public void render(float delta) {
        this.gameRenderer.draw();
        this.gameUpdater.update(delta);
    }

    @Override
    public void resize(int width, int height) {}

    @Override
    public void pause() {
        this.gameUpdater.doPause();
    }

    @Override
    public void resume() {}

    @Override
    public void hide() {
        dispose();
    }

    @Override
    public void dispose() {
        this.gameRenderer.dispose();
    }
}