package com.arkanoid.game;

import com.arkanoid.game.screen.MenuScreen;
import com.badlogic.gdx.Game;

/**
 * Класс-обёртка, являещийся входной точкой приложения
 */
public class ArkanoidGame extends Game {

	@Override
	public void create () {
		this.setScreen(new MenuScreen(this));
	}
}