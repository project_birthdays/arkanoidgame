package com.arkanoid.game.model.general;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class BackgroundPicture {
    private Texture texture;
    private Vector2 vector;

    public BackgroundPicture(Texture texture, float x, float y) {
        this.texture = texture;
        this.vector = new Vector2(x, y);
    }

    public Texture getTexture() {
        return this.texture;
    }
    public Vector2 getVector() {
        return this.vector;
    }
}