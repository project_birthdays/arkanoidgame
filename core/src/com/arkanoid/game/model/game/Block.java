package com.arkanoid.game.model.game;

import com.arkanoid.game.model.description.BlocksTypes;
import com.arkanoid.game.model.game.block.BlueBlock;
import com.arkanoid.game.model.game.block.GreenBlock;
import com.arkanoid.game.model.game.block.RedBlock;
import com.badlogic.gdx.graphics.Texture;

public class Block extends AbstractDynamicModel {
    private boolean destroyed;
    protected int points;

    public Block(){}
    public Block(float x, float y, float width, float height, String texturePath){
        super.setTexture(new Texture(texturePath));
        super.setX(x);
        super.setY(y);
        super.setWidth(width);
        super.setHeight(height);
        this.destroyed = false;
    }

    public Block init(float x, float y, float width, float height, BlocksTypes block) {
        switch (block.getType()) {
            case 0:
                return new BlueBlock(x, y, width, height);
            case 1:
                return new GreenBlock(x, y, width, height);
            case 2:
                return new RedBlock(x, y, width, height);
            default:
                break;
        }
        return null;
    }

    public int getPoints() {
        return this.points;
    }
    public boolean isDestroyed() {
        return this.destroyed;
    }
    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    @Override
    public void resetPosition() {
        super.resetPosition();
        this.destroyed = false;
    }
}