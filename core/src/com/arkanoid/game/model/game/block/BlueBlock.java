package com.arkanoid.game.model.game.block;

import com.arkanoid.game.model.game.Block;

public class BlueBlock extends Block {
    private static final String INTERNAL_TEXTURE_PATH = "image/block_blue.png";

    public BlueBlock(float x, float y, float width, float height) {
        super(x, y, width, height, INTERNAL_TEXTURE_PATH);
        super.points = 25;
    }
}