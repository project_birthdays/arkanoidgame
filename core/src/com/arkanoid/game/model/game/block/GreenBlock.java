package com.arkanoid.game.model.game.block;

import com.arkanoid.game.model.game.Block;

public class GreenBlock extends Block {
    private static final String INTERNAL_TEXTURE_PATH = "image/block_green.png";

    public GreenBlock(float x, float y, float width, float height) {
        super(x, y, width, height, INTERNAL_TEXTURE_PATH);
        super.points = 50;
    }
}