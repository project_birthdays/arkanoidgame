package com.arkanoid.game.model.game.block;

import com.arkanoid.game.model.game.Block;

public class RedBlock extends Block {
    private static final String INTERNAL_TEXTURE_PATH = "image/block_red.png";

    public RedBlock(float x, float y, float width, float height) {
        super(x, y, width, height, INTERNAL_TEXTURE_PATH);
        super.points = 75;
    }
}