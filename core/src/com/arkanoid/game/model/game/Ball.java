package com.arkanoid.game.model.game;

import com.arkanoid.game.model.description.BallDirection;

public class Ball extends AbstractDynamicModel {
    private static final float MIN_SPEED = 100.5f;
    private static final float MAX_SPEED = 250.5f;
    private static final float MAX_INCREMENT = 0.005f;
    private static final String INTERNAL_TEXTURE_PATH = "image/ball.png";
    private float speed;
    private BallDirection ballDirection;

    public Ball(float x, float y, float width, float height) {
        super(x, y, width, height, INTERNAL_TEXTURE_PATH);
        this.speed = MIN_SPEED;
        this.ballDirection = BallDirection.CENTER_DOWN;
    }

    @Override
    public void resetPosition() {
        super.resetPosition();
        this.ballDirection = BallDirection.CENTER_DOWN;
    }

    public BallDirection getBallDirection() {
        return ballDirection;
    }
    public void setBallDirection(BallDirection ballDirection) {
        this.ballDirection = ballDirection;
    }

    public void resetSpeed() {
        this.speed = MIN_SPEED;
    }
    public float getIncreaseSpeed() {
        if (this.speed < MAX_SPEED) {
            this.speed += MAX_INCREMENT;
        }
        return this.speed;
    }
}