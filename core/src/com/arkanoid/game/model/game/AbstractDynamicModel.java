package com.arkanoid.game.model.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;

public abstract class AbstractDynamicModel extends Rectangle {
    private float initX, initY;
    private Texture texture;

    AbstractDynamicModel() {}
    AbstractDynamicModel(float x, float y, float width, float height, String texturePath){
        super(x, y, width, height);
        this.texture = new Texture(texturePath);
        this.initX = x;
        this.initY = y;
    }

    void setTexture(Texture texture) {
        this.texture = texture;
    }
    public Texture getTexture() {
        return this.texture;
    }

    public void resetPosition() {
        super.x = initX;
        super.y = initY;
    }
}