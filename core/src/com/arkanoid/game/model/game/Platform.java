package com.arkanoid.game.model.game;

import com.arkanoid.game.model.description.PlatformDirection;

public class Platform extends AbstractDynamicModel {
    private static final float MIN_SPEED = 275.5f;
    private static final float MAX_SPEED = 450.5f;
    private static final float MAX_INCREMENT = 0.005f;
    private static final String INTERNAL_TEXTURE_PATH = "image/platform.png";
    private float speed;
    private PlatformDirection platformDirection;

    public Platform(float x, float y, float width, float height) {
        super(x, y, width, height, INTERNAL_TEXTURE_PATH);
        this.speed = MIN_SPEED;
        this.platformDirection = PlatformDirection.NONE;
    }

    public PlatformDirection getPlatformDirection() {
        return this.platformDirection;
    }
    public void setPlatformDirection(PlatformDirection platformDirection) {
        this.platformDirection = platformDirection;
    }

    @Override
    public void resetPosition() {
        super.resetPosition();
        this.speed = MIN_SPEED;
        this.platformDirection = PlatformDirection.NONE;
    }
    public void resetSpeed() {
        this.speed = MIN_SPEED;
    }

    public float getIncreaseSpeed() {
        if (this.speed < MAX_SPEED) {
            this.speed += MAX_INCREMENT;
        }
        return this.speed;
    }
}