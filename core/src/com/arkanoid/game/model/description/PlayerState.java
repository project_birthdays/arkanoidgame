package com.arkanoid.game.model.description;

/**
 * Перечисление для некоторых состояний игрока
 */
public enum PlayerState {
    READY,
    BEGINS,
    PLAYING,
    PLAYED;

    public boolean isReady() {
        return this == PlayerState.READY;
    }
    public boolean isBegins() {
        return this == PlayerState.BEGINS;
    }
    public boolean isPlaying() {
        return this == PlayerState.PLAYING;
    }
    public boolean isPlayed(){
        return this == PlayerState.PLAYED;
    }
}