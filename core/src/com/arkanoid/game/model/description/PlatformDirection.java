package com.arkanoid.game.model.description;

public enum  PlatformDirection {
    NONE,
    LEFT,
    RIGHT
}