package com.arkanoid.game.model.description;

public enum BallDirection {
    CENTER_DOWN,
    LEFT_DOWN,
    LEFT_UP,
    RIGHT_DOWN,
    RIGHT_UP;

    public boolean isCenterDown() {
        return this == CENTER_DOWN;
    }
    public boolean isLeftDown() {
        return this == LEFT_DOWN;
    }
    public boolean isLeftUp() {
        return this == LEFT_UP;
    }
    public boolean isRightDown() {
        return this == RIGHT_DOWN;
    }
    public boolean isRightUp() {
        return this == RIGHT_UP;
    }
}