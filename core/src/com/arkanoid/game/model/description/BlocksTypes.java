package com.arkanoid.game.model.description;

public enum BlocksTypes {
    BLUE_BLOCK(0),
    GREEN_BLOCK(1),
    RED_BLOCK(2);

    private int type;
    BlocksTypes(int type) {
        this.type = type;
    }

    public int getType() {
        return this.type;
    }
    public void setType(int type) {
        this.type = type;
    }
}