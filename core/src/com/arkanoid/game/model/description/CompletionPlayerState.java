package com.arkanoid.game.model.description;

public enum CompletionPlayerState {
    NONE,
    NO_BLOCKS,
    NO_HEALTH,
    NO_ROUND_TIME;

    public boolean isNone() {
        return this == NONE;
    }
    public boolean isNoBlocks() {
        return this == NO_BLOCKS;
    }
    public boolean isNoHealth() {
        return this == NO_HEALTH;
    }
    public boolean isNoRoundTime() {
        return this == NO_ROUND_TIME;
    }
}