package com.arkanoid.game.model.description;

public enum GameResultState {
    GOOSE_WON,
    WOLF_WON,
    DRAW;

    public boolean isGooseWon() {
        return this == GOOSE_WON;
    }
    public boolean isWolfWon() {
        return this == WOLF_WON;
    }
    public boolean isDraw() {
        return this == DRAW;
    }
}