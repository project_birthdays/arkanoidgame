package com.arkanoid.game.model.description;

/**
 * Перечисление для некоторых состояний игры
 */
public enum GameState {
    PREP,
    RUNNING,
    PAUSE,
    COMPLETION,
    CHANGE_PLAYER,
    RESULT,
    RESTART;

    public boolean isPrep() {
        return this == GameState.PREP;
    }
    public boolean isRunning() {
        return this == GameState.RUNNING;
    }
    public boolean isPause() {
        return this == GameState.PAUSE;
    }
    public boolean isCompletion() {
        return this == GameState.COMPLETION;
    }
    public boolean isChangePlayer() {
        return this == GameState.CHANGE_PLAYER;
    }
    public boolean isResult() {
        return this == GameState.RESULT;
    }
    public boolean isRestart() {
        return this == GameState.RESTART;
    }
}