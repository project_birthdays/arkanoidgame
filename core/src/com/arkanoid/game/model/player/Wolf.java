package com.arkanoid.game.model.player;

public class Wolf extends AbstractPlayerModel {
    private static final String WOLF_PLAYER = "image/wolf_player.png";
    private static final String PLAYER_NAME = "Волк";

    public Wolf(){
        super(WOLF_PLAYER, PLAYER_NAME);
    }
}