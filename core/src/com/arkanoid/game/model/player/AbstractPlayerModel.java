package com.arkanoid.game.model.player;

import com.arkanoid.game.model.description.PlayerState;
import com.arkanoid.game.util.TimeFormat;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class AbstractPlayerModel {
    private static final float DEFAULT_RUN_TIME = 0.f;
    private Sprite sprite;
    private PlayerState playerState;
    private String name;
    private int points;
    private byte health;
    private float runtime;

    AbstractPlayerModel(String path, String name){
        Texture texture = new Texture(path);
        this.sprite = new Sprite(texture);
        this.playerState = PlayerState.READY;
        this.name = name;
        this.points = 0;
        this.health = 5;
        this.runtime = DEFAULT_RUN_TIME;
    }

    public void updateProperties() {
        this.playerState = PlayerState.READY;
        this.points = 0;
        this.health = 5;
        this.runtime = DEFAULT_RUN_TIME;
    }

    public Sprite getSprite() {
        return this.sprite;
    }
    public int getPoints() {
        return this.points;
    }
    public byte getHealth() {
        return this.health;
    }
    public PlayerState getPlayerState() {
        return this.playerState;
    }

    public void setPlayerState(PlayerState playerState) {
        this.playerState = playerState;
    }
    public int getRuntimeInInt() {
        return (int) this.runtime;
    }

    public void increaseRunTime(float delta) {
        this.runtime += delta;
    }
    public void increasePoints(int points){
        this.points += points;
    }
    public void reduceHealth(){
        this.health -= 1;
    }
    public boolean isNoPlayerHealth(){
        return this.health == 0;
    }

    @Override
    public String toString() {
        return name + ": " + points + " очк., " + health + " жзн., " + TimeFormat.getTimeInString(runtime) + " вр.";
    }
}