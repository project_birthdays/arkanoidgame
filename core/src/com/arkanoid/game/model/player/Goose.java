package com.arkanoid.game.model.player;

public class Goose extends AbstractPlayerModel {
    private static final String GOOSE_PLAYER = "image/goose_player.png";
    private static final String PLAYER_NAME = "Гусь";

    public Goose() {
        super(GOOSE_PLAYER, PLAYER_NAME);
    }
}