package com.arkanoid.game.controller.game;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.AbstractDynamicController;
import com.arkanoid.game.model.description.PlatformDirection;
import com.arkanoid.game.model.game.Platform;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Класс предназначен для отображения, обновления и высвобождения ресурсов
 * элемента платформы на игровом экране приложения
 *
 * Класс-контроллер, описывающий поведение платформы на игровом экране приложения
 */
public class PlatformController extends AbstractDynamicController implements SizeFactor {
    private Platform platform;
    private GameUpdater gameUpdater;
    private OrthographicCamera orthographicCamera;
    private Vector3 touchPosition;

    public PlatformController(GameUpdater gameUpdater) {
        this.gameUpdater = gameUpdater;
        this.platform = gameUpdater.getPlatform();
        this.orthographicCamera = gameUpdater.getOrthographicCamera();
        this.touchPosition = new Vector3();
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        spriteBatch.draw(platform.getTexture(), platform.getX(), platform.getY());
    }

    @Override
    public void updateElement(float delta) {
        handleTouch();
        movePlatformByDirection(delta, platform.getIncreaseSpeed());

        if(platform.getX() < 0) {
            platform.setX(0);
        }
        if(platform.getX() > MAX_VIEW_WIDTH - PLATFORM_WIDTH) {
            platform.setX(MAX_VIEW_WIDTH - PLATFORM_WIDTH);
        }
    }

    @Override
    public void disposeElement() {
        platform.getTexture().dispose();
    }

    @Override
    public void onRestart() {
        platform.resetPosition();
        platform.resetSpeed();
    }

    private void handleTouch() {
        this.touchPosition.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        this.orthographicCamera.unproject(touchPosition);
        float touchX = touchPosition.x;
        float touchY = touchPosition.y;
        if(Gdx.input.isTouched()) {
            if (gameUpdater.isLeftButton(touchX, touchY)) {
                platform.setPlatformDirection(PlatformDirection.LEFT);
            } else if (gameUpdater.isRightButton(touchX, touchY)) {
                platform.setPlatformDirection(PlatformDirection.RIGHT);
            }
        } else {
            platform.setPlatformDirection(PlatformDirection.NONE);
        }
    }
    private void movePlatformByDirection(float delta, float speed) {
        switch (platform.getPlatformDirection()) {
            case LEFT:
                gameUpdater.getPlatform().setX(gameUpdater.getPlatform().getX() - speed * delta);
                break;
            case RIGHT:
                gameUpdater.getPlatform().setX(gameUpdater.getPlatform().getX() + speed * delta);
                break;
            default:
                break;
        }
    }
}