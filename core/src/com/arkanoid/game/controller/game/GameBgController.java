package com.arkanoid.game.controller.game;

import com.arkanoid.game.controller.general.BackgroundController;
import com.arkanoid.game.model.general.BackgroundPicture;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameBgController extends BackgroundController {
    private BackgroundPicture[] backgroundPictures;
    private float offsetSpeed;

    public GameBgController(GameUpdater gameUpdater) {
        super(gameUpdater);
        this.backgroundPictures = getBackgroundPictures();
        this.offsetSpeed = 0.95f;
    }

    @Override
    public void updateElement(float delta) {
        for (BackgroundPicture backgroundPicture : backgroundPictures) {
            backgroundPicture.getVector().x -= this.offsetSpeed;
        }
        if (backgroundPictures[0].getVector().x < -800) {
            backgroundPictures[0].getVector().x = 0;
            backgroundPictures[1].getVector().x = 800;
        }
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        for (BackgroundPicture backgroundPicture : backgroundPictures) {
            spriteBatch.draw(backgroundPicture.getTexture(), backgroundPicture.getVector().x, backgroundPicture.getVector().y);
        }
    }

    @Override
    public void disposeElement() {
        for (BackgroundPicture backgroundPicture : backgroundPictures) {
            backgroundPicture.getTexture().dispose();
        }
    }
}