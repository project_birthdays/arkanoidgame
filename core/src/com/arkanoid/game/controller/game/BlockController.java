package com.arkanoid.game.controller.game;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.AbstractDynamicController;
import com.arkanoid.game.model.description.BlocksTypes;
import com.arkanoid.game.model.game.Block;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;

/**
 * Класс предназначен для отображения, обновления и высвобождения ресурсов
 * элемента блока на игровом экране приложения
 *
 * Класс-контроллер, описывающий поведение блоков на игровом экране приложения
 */
public class BlockController extends AbstractDynamicController implements SizeFactor {
    private GameUpdater gameUpdater;
    private Pool<Block> blockPool;
    private Array<Block> blockArray;

    public BlockController(GameUpdater gameUpdater) {
        this.gameUpdater = gameUpdater;
        this.blockPool = gameUpdater.getBlockPool();
        this.blockArray = gameUpdater.getBlockArray();
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        for (Block block : blockArray) {
            spriteBatch.draw(block.getTexture(), block.getX(), block.getY());
        }
    }

    @Override
    public void updateElement(float delta) {
        if (!blockArray.isEmpty()) {
            freeDestroyedBlocks();
        }
    }

    @Override
    public void disposeElement() {
        for (Block block : blockArray) {
            block.getTexture().dispose();
        }
    }

    @Override
    public void onRestart() {
        blockPool.freeAll(blockArray);
        blockArray.clear();
        if (blockArray.isEmpty() && (gameUpdater.getGameState().isChangePlayer() | gameUpdater.getGameState().isRestart())) {
            initBlocks();
        }
    }

    public void initBlocks() {
        for(int i = (int) (MAX_VIEW_WIDTH / 100); i < MAX_VIEW_WIDTH; i += (int)MAX_VIEW_WIDTH / 15) {
            blockArray.add(blockPool.obtain().init(i, (((MAX_VIEW_HEIGHT / 2) + (MAX_VIEW_HEIGHT / 5 ) + 2)), BLOCK_WIDTH, BLOCK_HEIGHT, BlocksTypes.RED_BLOCK));
            blockArray.add(blockPool.obtain().init(i, ((MAX_VIEW_HEIGHT / 2) + (MAX_VIEW_HEIGHT / 10) + 2), BLOCK_WIDTH, BLOCK_HEIGHT, BlocksTypes.GREEN_BLOCK));
            blockArray.add(blockPool.obtain().init(i, ((MAX_VIEW_HEIGHT / 2) + 2), BLOCK_WIDTH, BLOCK_HEIGHT, BlocksTypes.BLUE_BLOCK));
        }
    }
    private void freeDestroyedBlocks() {
        Block block;
        for (int i = blockArray.size; --i >= 0;) {
            block = blockArray.get(i);
            if (block.isDestroyed()) {
                blockArray.removeIndex(i);
                blockPool.free(block);
            }
        }
    }
}