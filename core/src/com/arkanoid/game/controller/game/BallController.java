package com.arkanoid.game.controller.game;

import com.arkanoid.game.controller.AbstractDynamicController;
import com.arkanoid.game.model.description.BallDirection;
import com.arkanoid.game.model.game.Ball;
import com.arkanoid.game.model.game.Block;
import com.arkanoid.game.render.game.GameEvents;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Класс предназначен для отображения, обновления и высвобождения ресурсов
 * элемента мяча на игровом экране приложения
 *
 * Класс-контроллер, описывающий поведение мяча на игровом экране приложения
 */
public class BallController extends AbstractDynamicController {
    private Ball ball;
    private GameUpdater gameUpdater;
    private GameEvents gameEvents;

    public BallController(GameUpdater gameUpdater) {
        this.ball = gameUpdater.getBall();
        this.gameUpdater = gameUpdater;
        this.gameEvents = gameUpdater.getGameEvents();
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        spriteBatch.draw(ball.getTexture(), ball.getX(), ball.getY());
    }

    @Override
    public void updateElement(float delta) {
        if (gameUpdater.isUpBorderOnPlatform() && ball.getBallDirection().isCenterDown()) {
            if (gameUpdater.isRightBorderOnPlatform()) {
                gameEvents.borderCollisionFactor();
                this.ball.setBallDirection(BallDirection.RIGHT_UP);
            } else if (gameUpdater.isLeftBorderOnPlatform()){
                gameEvents.borderCollisionFactor();
                this.ball.setBallDirection(BallDirection.LEFT_UP);
            }
        }
        if (gameUpdater.isUpBorderOnPlatform() && ball.getBallDirection().isLeftDown()) {
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.LEFT_UP);
        } else if (gameUpdater.isUpBorderOnPlatform() && ball.getBallDirection().isRightDown()) {
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.RIGHT_UP);
        }
        if (gameUpdater.isLeftBorderOnScreen() && ball.getBallDirection().isLeftUp()) {
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.RIGHT_UP);
        } else if (gameUpdater.isLeftBorderOnScreen() && ball.getBallDirection().isLeftDown()){
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.RIGHT_DOWN);
        }
        if (gameUpdater.isRightBorderOnScreen() && ball.getBallDirection().isRightUp()) {
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.LEFT_UP);
        } else if (gameUpdater.isRightBorderOnScreen() && ball.getBallDirection().isRightDown()){
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.LEFT_DOWN);
        }
        if (gameUpdater.isUpBorderOnScreen() && ball.getBallDirection().isRightUp()) {
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.RIGHT_DOWN);
        } else if (gameUpdater.isUpBorderOnScreen() && ball.getBallDirection().isLeftUp()){
            gameEvents.borderCollisionFactor();
            this.ball.setBallDirection(BallDirection.LEFT_DOWN);
        }

        for (Block block : gameUpdater.getBlockArray()) {
            if (gameUpdater.isLeftBlockCollision(ball, block) && ball.getBallDirection().isRightUp()) {
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.LEFT_UP);
            } else if (gameUpdater.isLeftBlockCollision(ball, block) && ball.getBallDirection().isRightDown()){
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.LEFT_DOWN);
            } else if (gameUpdater.isDownBlockCollision(ball, block) && ball.getBallDirection().isLeftUp()) {
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.LEFT_DOWN);
            } else if (gameUpdater.isDownBlockCollision(ball, block) && ball.getBallDirection().isRightUp()){
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.RIGHT_DOWN);
            } else if (gameUpdater.isRightBlockCollision(ball, block) && ball.getBallDirection().isLeftUp()) {
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.RIGHT_UP);
            } else if (gameUpdater.isRightBlockCollision(ball, block) && ball.getBallDirection().isLeftDown()){
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.RIGHT_DOWN);
            } else if (gameUpdater.isUpBlockCollision(ball, block) && ball.getBallDirection().isLeftDown()) {
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.LEFT_UP);
            } else if (gameUpdater.isUpBlockCollision(ball, block) && ball.getBallDirection().isRightDown()){
                gameEvents.blockCollisionFactor(block);
                this.ball.setBallDirection(BallDirection.RIGHT_UP);
            }
        }
        moveBallByDirection(ball.getIncreaseSpeed(), delta);
    }

    @Override
    public void disposeElement() {
        ball.getTexture().dispose();
    }

    @Override
    public void onRestart() {
        ball.resetPosition();
        ball.resetSpeed();
    }

    public boolean isLowFactorPosition() {
        return ball.getY() + ball.getHeight() < 0;
    }

    private void moveBallByDirection(float speed, float delta) {
        switch (ball.getBallDirection()) {
            case CENTER_DOWN:
                ball.setY(ball.getY() - speed * delta);
                break;
            case LEFT_DOWN:
                ball.setX(ball.getX() - speed * delta);
                ball.setY(ball.getY() - speed * delta);
                break;
            case LEFT_UP:
                ball.setX(ball.getX() - speed * delta);
                ball.setY(ball.getY() + speed * delta);
                break;
            case RIGHT_DOWN:
                ball.setX(ball.getX() + speed * delta);
                ball.setY(ball.getY() - speed * delta);
                break;
            case RIGHT_UP:
                ball.setX(ball.getX() + speed * delta);
                ball.setY(ball.getY() + speed * delta);
                break;
        }
    }
}