package com.arkanoid.game.controller;

public abstract class AbstractDynamicController implements ScreenElementListener {
    public abstract void onRestart();
}