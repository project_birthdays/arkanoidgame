package com.arkanoid.game.controller;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Интерфейс-слушатель предназначен для отображения, обновления и высвобождения ресурсов
 * отдельного элемента на экране приложения
 *
 * Имплементируется классами-контроллерами, описывающими поведение отдельного
 * элемента на экране приложения
 */
public interface ScreenElementListener {
    void drawElement(SpriteBatch spriteBatch);
    void updateElement(float delta);
    void disposeElement();
}