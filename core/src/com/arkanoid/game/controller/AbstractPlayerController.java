package com.arkanoid.game.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class AbstractPlayerController implements ScreenElementListener {
    private static final String INTERNAL_FONT_PATH = "font/verdana_font.fnt";
    protected BitmapFont bitmapFont;

    protected AbstractPlayerController() {
        this.bitmapFont = new BitmapFont(Gdx.files.internal(INTERNAL_FONT_PATH));
    }
    public abstract void drawScore(SpriteBatch spriteBatch);
    public abstract void drawLive(SpriteBatch spriteBatch);
    public abstract void drawPicture(SpriteBatch spriteBatch);
}