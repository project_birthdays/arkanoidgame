package com.arkanoid.game.controller.general;

import com.arkanoid.game.controller.ScreenElementListener;
import com.arkanoid.game.model.general.BackgroundPicture;
import com.arkanoid.game.render.game.GameUpdater;
import com.arkanoid.game.render.menu.MenuUpdater;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BackgroundController implements ScreenElementListener {
    private static final String INTERNAL_DESTROY_MUSIC_MENU_PATH = "music/background.mp3";
    private static final String INTERNAL_DESTROY_MUSIC_GAME_PATH = "music/background_game.mp3";
    private BackgroundPicture backgroundPicture;
    private BackgroundPicture[] backgroundPictures;
    private Music musicMenu;

    protected BackgroundPicture[] getBackgroundPictures() {
        return backgroundPictures;
    }

    public BackgroundController(GameUpdater gameUpdater) {
        backgroundPictures = gameUpdater.getBackgroundPictures();
        this.musicMenu = Gdx.audio.newMusic(Gdx.files.internal(INTERNAL_DESTROY_MUSIC_GAME_PATH));
        musicMenu.setLooping(true);
        musicMenu.play();
    }

    public BackgroundController(MenuUpdater menuUpdater){
        backgroundPicture = menuUpdater.getBackgroundPicture();
        this.musicMenu = Gdx.audio.newMusic(Gdx.files.internal(INTERNAL_DESTROY_MUSIC_MENU_PATH));
        musicMenu.setLooping(true);
        musicMenu.play();
    }

    @Override
    public void drawElement(SpriteBatch spriteBatch) {
        spriteBatch.draw(backgroundPicture.getTexture(), backgroundPicture.getVector().x, backgroundPicture.getVector().y);
    }

    @Override
    public void updateElement(float delta) {}

    @Override
    public void disposeElement() {
        musicMenu.dispose();
        backgroundPicture.getTexture().dispose();
    }
}