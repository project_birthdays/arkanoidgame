package com.arkanoid.game.controller.player;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.AbstractPlayerController;
import com.arkanoid.game.model.player.Wolf;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class WolfController extends AbstractPlayerController implements SizeFactor {
    private static final int MAX_WIDTH_ICON_PERSON = 100, MAX_HEIGHT_ICON_PERSON = 80;
    private GameUpdater gameUpdater;
    private Wolf wolf;

    public WolfController(GameUpdater gameUpdater) {
        this.gameUpdater = gameUpdater;
        this.wolf = gameUpdater.getWolf();
    }
    @Override
    public void drawElement(final SpriteBatch spriteBatch) {
        if (gameUpdater.getGameState().isPrep() && (gameUpdater.getWolf().getPlayerState().isBegins()
                | (gameUpdater.getGoose().getPlayerState().isPlayed() && gameUpdater.getWolf().getPlayerState().isReady()))) {
            bitmapFont.draw(spriteBatch, "Играет волк! ", MAX_VIEW_WIDTH / 2.7f, MAX_VIEW_HEIGHT / 1.15f);
            bitmapFont.draw(spriteBatch, "Нажмите на экран, чтобы играть!", MAX_VIEW_WIDTH / 7.0f, MAX_VIEW_HEIGHT / 2.1f);
        }
        if ((gameUpdater.getWolf().getPlayerState().isBegins() | gameUpdater.getWolf().getPlayerState().isPlaying()
                | (gameUpdater.getGoose().getPlayerState().isPlayed() && gameUpdater.getWolf().getPlayerState().isReady()))
                && !gameUpdater.getWolf().getPlayerState().isPlayed() && !gameUpdater.getGameState().isCompletion()
                && !(gameUpdater.getGameState().isPause() && (gameUpdater.getPrevPauseGameState().isCompletion()))) {
            drawScore(spriteBatch);
            drawLive(spriteBatch);
            drawPicture(spriteBatch);
        }
    }

    @Override
    public void updateElement(float delta) {}

    @Override
    public void disposeElement() {
        super.bitmapFont.dispose();
        this.wolf.getSprite().getTexture().dispose();
    }

    @Override
    public void drawScore(SpriteBatch spriteBatch) {
        bitmapFont.draw(spriteBatch, String.valueOf(this.wolf.getPoints()), MAX_VIEW_WIDTH / 1.15f, MAX_VIEW_HEIGHT / 1.15f);
    }

    @Override
    public void drawLive(SpriteBatch spriteBatch) {
        bitmapFont.draw(spriteBatch, "x" + this.wolf.getHealth(), MAX_VIEW_WIDTH / 5.2f, MAX_VIEW_HEIGHT / 1.15f);
    }

    @Override
    public void drawPicture(SpriteBatch spriteBatch) {
        wolf.getSprite().draw(spriteBatch);
        wolf.getSprite().setSize(MAX_WIDTH_ICON_PERSON, MAX_HEIGHT_ICON_PERSON);
        wolf.getSprite().setPosition(MAX_VIEW_WIDTH / 75.5f, MAX_VIEW_HEIGHT / 1.26f);
    }
}