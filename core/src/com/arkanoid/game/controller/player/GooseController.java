package com.arkanoid.game.controller.player;

import com.arkanoid.game.SizeFactor;
import com.arkanoid.game.controller.AbstractPlayerController;
import com.arkanoid.game.model.player.Goose;
import com.arkanoid.game.render.game.GameUpdater;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GooseController extends AbstractPlayerController implements SizeFactor {
    private static final int MAX_WIDTH_ICON_PERSON = 100, MAX_HEIGHT_ICON_PERSON = 80;
    private GameUpdater gameUpdater;
    private Goose goose;

    public GooseController(GameUpdater gameUpdater) {
        this.gameUpdater = gameUpdater;
        this.goose = gameUpdater.getGoose();
    }
    @Override
    public void drawElement(final SpriteBatch spriteBatch) {
        if (gameUpdater.getGameState().isPrep() && (gameUpdater.getGoose().getPlayerState().isBegins()
                | (gameUpdater.getWolf().getPlayerState().isPlayed() && gameUpdater.getGoose().getPlayerState().isReady()))) {
            bitmapFont.draw(spriteBatch, "Играет гусь! ", MAX_VIEW_WIDTH / 2.7f, MAX_VIEW_HEIGHT / 1.15f);
            bitmapFont.draw(spriteBatch, "Нажмите на экран, чтобы играть!", MAX_VIEW_WIDTH / 7.0f, MAX_VIEW_HEIGHT / 2.1f);
        }
        if ((gameUpdater.getGoose().getPlayerState().isBegins() | gameUpdater.getGoose().getPlayerState().isPlaying()
                | (gameUpdater.getWolf().getPlayerState().isPlayed() && gameUpdater.getGoose().getPlayerState().isReady()))
                && !gameUpdater.getGoose().getPlayerState().isPlayed() && !gameUpdater.getGameState().isCompletion()
                && !(gameUpdater.getGameState().isPause() && gameUpdater.getPrevPauseGameState().isCompletion())) {
            drawScore(spriteBatch);
            drawLive(spriteBatch);
            drawPicture(spriteBatch);
        }
    }

    @Override
    public void updateElement(float delta) {}

    @Override
    public void disposeElement() {
        super.bitmapFont.dispose();
        this.goose.getSprite().getTexture().dispose();
    }

    @Override
    public void drawScore(SpriteBatch spriteBatch) {
        bitmapFont.draw(spriteBatch, String.valueOf(this.goose.getPoints()), MAX_VIEW_WIDTH / 1.15f, MAX_VIEW_HEIGHT / 1.15f);
    }

    @Override
    public void drawLive(SpriteBatch spriteBatch) {
        bitmapFont.draw(spriteBatch, "x" + this.goose.getHealth(), MAX_VIEW_WIDTH / 5.2f, MAX_VIEW_HEIGHT / 1.15f);
    }

    @Override
    public void drawPicture(SpriteBatch spriteBatch) {
        goose.getSprite().draw(spriteBatch);
        goose.getSprite().setSize(MAX_WIDTH_ICON_PERSON, MAX_HEIGHT_ICON_PERSON);
        goose.getSprite().setPosition(MAX_VIEW_WIDTH / 75.5f, MAX_VIEW_HEIGHT / 1.26f);
    }
}